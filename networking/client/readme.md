# Using client static file sharing

## Install platform

**Linux** with `apt` package manager

```bash
sudo apt update
sudo apt install -y nodejs npm
npm i -g yarn
```

**Windows** with `winget` package manager

```ps1
winget install OpenJS.NodeJS
# you might need to restart
npm i -g yarn
```

## Install dependencies

Move into `/client` folder

```bash
yarn install
```

## Start server

Move into `/client` folder

```bash
yarn serve
```