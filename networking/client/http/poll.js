const host = window.location.origin
const apiPort = 8000
const apiEndpoint = 'poll'
const apiURL = `${host}:${apiPort}/${apiEndpoint}`

function startFetch(timing) {
    return setInterval(async () => {
        let time = performance.now()
        let res = await fetch(apiURL)
        let json = await res.json()

        measure = performance.now() - time
        measurements.push(measure)
        console.log( measure )
        
        window.cycle.innerText = measurements.length.toString()
        window.output.innerText = JSON.stringify(json, null, 3)
    }, timing)
}