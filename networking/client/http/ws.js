const host = window.location.hostname
const apiPort = 8040
const apiEndpoint = 'websocket'
const apiURL = `ws://${host}:${apiPort}/${apiEndpoint}`

function startWs(timing) {
    const ws = new WebSocket(apiURL)
    let time = 0
    let request = JSON.stringify({req:true})

    ws.onmessage = function(event) {
        let data = JSON.parse(event.data)
        
        measure = performance.now() - time
        measurements.push(measure)
        console.log( measure )
        
        window.cycle.innerText = measurements.length.toString()
        window.output.innerText = JSON.stringify(data, null, 3)
    }

    return setInterval(() => {
        time = performance.now()
        ws.send( request )
    }, timing)
}