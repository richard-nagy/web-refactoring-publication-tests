## Networking Benchmarks


### 1. Activate virtual environment

> IMPORTANT: The source keyword is necessary to properly activate the virtual environment.

```bash
source install.sh
```

### 2. Start client hosting

```bash
cd client
yarn install
yarn serve
```

### 3. Start a test server

You may start them all at once in different terminals

Polling (port 8000)

```bash
python poll.py
```

Websocket (port 8040)

```bash
python websocket.py
```

WebRTC (port 8080)

```bash
python webrtc.py
```

### Deactivate

Close servers, then deactivate the virtual environment

```bash
deactivate
```