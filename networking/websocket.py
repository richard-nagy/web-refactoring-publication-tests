import json
import uvicorn
from fastapi import FastAPI
from fastapi import Request
from fastapi import WebSocket
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

PORT = 8040

# Setup app
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Read data json
with open('./data/image.json', 'r') as file:
    data = json.loads(file.read())


# Websocket connection
@app.websocket("/websocket")
async def ws_websocket(websocket: WebSocket):
    await websocket.accept()
    while True:
        await websocket.receive_text()
        await websocket.send_json(data)
       
if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=PORT, reload=False)