# Refactoring Legacy Applications Publication

This publication is about refactoring legacy applications in a microservice model. In particular, we were working on a medical application with Mediso Medical Imaging Systems. This repository acts as a supplement for the publication.

[[_TOC_]]

## Requirements

All of the examples and sample shell codes are written in **bash** for Linux systems. We recommend you to use a linux system for testing.

### Software

List of software requirements with the versions used during development.

- GNU Bash (`5.0.17`)
- NodeJS (`v14.18.2`)
- NPM (`v6.14.15`)
- Yarn (`1.22.17`)
- Python (`3.10.3`)
- Pip (`22.0.4`)

## Code Acquisition

### Git Cloning

Git must be installed on the system. The repository is public, so no password will be necessary.

```bash
git clone https://gitlab.com/richardnagy/research/refactoring-legacy-publication
cd refactoring-legacy-publication
```

### Zip Download

1. Open the following page: [repository](https://gitlab.com/richardnagy/research/refactoring-legacy-publication)
2. Click on download
3. Click on zip

Or use this direct link: [zip](https://gitlab.com/richardnagy/research/refactoring-legacy-publication/-/archive/main/refactoring-legacy-publication-main.zip)




## Compression Benchmark

The compression benchmark only considers the networking transmission time based on different methods of compression. The results are not conclusive and not absolute. Instead, the relative differences between compression methods are to be considered.

More information in the [Compression](./compression/) section.




## Networking Benchmark

The networking benchmark aims to compare the speed between **polling**, **WebSocket** and **WebRTC** raw latency performance. These tests are not identical to real-world performance, since for the sake of consistency, each package send was initiated with a request (which is not necessary in case of WebSocket and WebRTC). Because of this, the real performance will be considerably higher than the one experienced here.

More information in the [Networking](./networking/) section.
